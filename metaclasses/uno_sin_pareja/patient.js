"use strict";

class Patient_Meta extends Common{
    constructor(...class_params){
	super(...class_params);
	this.rect=undefined;
    }

    i_am_supervisor(){
	return false;
    }        

    process_message(x){
	this.update(x);
    }
}

