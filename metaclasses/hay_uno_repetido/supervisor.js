"use strict";


class Supervisor_Meta extends Common{

    constructor(...class_params){
	super(...class_params);
	this.add_controls(true,false,false);
	this.Ncycles=1;
	this.min_slide=0;	
    }

    i_am_supervisor(){
	return true;
    }
    
    sample(an_array,N){
	// Takes a random subsample of size N from an_array	
	if (N > an_array.length){	    
	    throw `N should be smaller or equal to the array length. ${N} > ${an_array.length}`;
	}
	var an_array=an_array.slice()
	var return_array=[]
	while(N>0){
	    let random_element = Math.floor(Math.random() * (an_array.length)) ;
	    return_array.push(an_array.splice(random_element,1));
	    N--;
	}
	return return_array
    }

    create_slide_params(N,number_of_images,ob){
	var grid_positions=ob.sample(Array.from({length: ob.grid_size} , (v,i) => {return i}),N+1);
	var images=ob.sample(Array.from({length: number_of_images} , (v,i) => {return i}),N);
	var non_paired_info=[];
	for(let i=0;i<N-1;i++){	    
	    non_paired_info.push([images[i],grid_positions[i]]);
	}
	var paired_info = [images[N-1],grid_positions[N-1],grid_positions[N]];
	return [non_paired_info,paired_info,ob.cycle];
    }

    set_up_cycle(){
	
	var number_of_images=this.cycles_info[this.cycle-1].number_of_images;
	var Nimages=this.cycles_info[this.cycle-1].Nimages;
	
	var this_aux=this;
	this.enumerate_slides=Array.from(Nimages,(v,i) =>{return this_aux.create_slide_params(v,number_of_images,this_aux)});
    }

    get_answer(n){
	var image_index_1 = this.enumerate_slides[n][1][1];
	var image_index_2 = this.enumerate_slides[n][1][2];
	this.images[image_index_1].style.border=`solid #aa00aa ${this.border_size}px`;
	this.images[image_index_2].style.border=`solid #aa00aa ${this.border_size}px`;	
    }
}


  
