"use strict";

class Common extends slidesExercise{
    
    constructor(nrows,ncolumns,exercise_folder_name){
	super();
	this.set_vars(nrows,ncolumns,exercise_folder_name);
	if(this.border_size == undefined){
	    this.border_size=0;
	}
	var this_aux=this;
	window.onresize = ()=>{
	    this_aux.set_images_size();
	};	
    }

    set_vars(nrows,ncolumns,exercise_folder_name){
	//It is assumed that the set of pictures for each cycle is in a
	//folder called setN where N is the cicle number inside the folder
	//exercise_folder_name
	this.nrows=nrows;
	this.ncolumns=ncolumns;
	this.exercise_folder_name=exercise_folder_name;
	this.create_image_grid();
    }

    set_image_css(an_image){
	var hw_ratio = an_image.naturalHeight / an_image.naturalWidth;	
	
	if( this.max_hw_r >= hw_ratio  ){
	    an_image.style.maxWidth=`${this.image_max_width}px`;
	    an_image.style.maxHeight="";
	}else{
	    an_image.style.maxWidth="";
	    an_image.style.maxHeight=`${this.image_max_height}px`;
	}
	an_image.style.display="block";
    }

    set_images_size(){
	this.slide_rect = this.div.getBoundingClientRect();
	this.slide_height = this.slide_rect.height;
	this.slide_width = this.slide_rect.width;
	this.wide_screen=this.slide_width > this.slide_height;
	this.image_max_width = Math.floor(this.slide_width / this.ncolumns - 4*this.border_size);
	this.image_max_height = Math.floor( this.slide_height / this.nrows - 4*this.border_size);
	this.max_hw_r= this.image_max_height / this.image_max_width;
	for(let image of this.images){
	    this.set_image_css(image);
	}
    }


    create_image_grid(){
	this.canvas = this.add_element("div","canvas","canvas",this.slide);
	this.canvas.style.gridTemplateColumns=`repeat(${this.ncolumns},1fr)`;
	this.canvas.style.gridTemplateRows=`repeat(${this.nrows},1fr)`;
	this.images=[];	
	for(let row=0;row < this.nrows;row++){
	    for (let column=0;column < this.ncolumns;column++){
		let image_number = row*this.ncolumns + column + 1;
		let picture=this.add_element("img","image",`image${image_number}`,this.canvas);
		picture.style.gridRow=row+1;
		picture.style.gridColumn=column+1;
		picture.style.display="none";
		this.images.push(picture);
		picture.onload=()=>{console.log(`Loaded image ${image_number}`)};
	    }
	}
    }

    display_image(Nimage,Ncell,set){
	this.images[Ncell].src=`/exercises/${this.exercise_folder_name}/set${set}/${Nimage}.svg`;
	// this.images[Ncell].style.display="block";
	var this_image=this.images[Ncell];
	return new Promise((resolve,reject)=>{
	    this_image.onload=()=>{resolve(1)};
	});
	
    }

    

    show_slide(non_paired_indexes,pair_index,set){
	for(let image of this.images){
	    image.src="";
	    image.style.display="none";
	    image.style.border="";
	    image.style.maxHeight="";
	    image.style.maxWidth="";
	}
	var images_promises=non_paired_indexes.map(a_non_pair => this.display_image(a_non_pair[0],a_non_pair[1],set));
	images_promises.push(this.display_image(pair_index[0],pair_index[1],set));
	images_promises.push( this.display_image(pair_index[0],pair_index[2],set));
	let this_aux=this;
	Promise.all(images_promises).then(()=>{this.set_images_size()});
    }
    
}
