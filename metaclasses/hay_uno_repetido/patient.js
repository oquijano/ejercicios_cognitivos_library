"use strict";

class Patient_Meta extends Common{
    constructor(...class_params){
	super(...class_params);
	this.rect=undefined;
	this.div.style.marginTop="10vh";
	this.canvas.style.height="70vh";
    }

    i_am_supervisor(){
	return false;
    }
    
    process_message(x){
	this.update(x);
    }
}

