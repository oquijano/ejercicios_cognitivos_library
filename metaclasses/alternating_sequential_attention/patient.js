"use strict";

class Patient_Meta extends AlternatingSequentialAttention{
    
    constructor(){
	super();
    }

    i_am_supervisor(){
	return false;
    }

    process_message(x){
	this.update(x);
    }
}

