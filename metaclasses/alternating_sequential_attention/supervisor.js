"use strict";

class Supervisor_Meta extends AlternatingSequentialAttention{
    constructor(){
	super();
	
	this.add_controls(true,true,true);	
	this.Ncycles = 1;
	
	this.min_slide=2;
	this.slide_number_label=undefined;
	
    }
    
    async choose_word(){
	await this.meta_locale.finished_loading;
	var current_locale=this.meta_locale.i18n.getLocale();
	var word_file=this.meta_locale.i18n.gettext(`words_${current_locale}.txt`);
	
	var url_list=window.location.href.split("/");
	var exercise_folder_name = url_list[url_list.length -2];

	var full_url = `/exercises/${exercise_folder_name}/${word_file}`;

	const response = await fetch(full_url);
	const text = await response.text();

	const words = text.split(/ *\n/);
		
	this.word = words[this.get_random_int(0 , words.length - 1 )];
    }


    generate_numeric_sequence(size){
	// size : integer, lenght of the sequence	
	var reference=this.get_random_int(1,30);
	var delta=[3,4,5,6][this.get_random_int(0,3)];
	
	return Array.from({length : size},
			  (v,i) => reference + i * delta);
    }

    add_element_and_translation(type,css_class,id,parent,translation_thunk){
	
	var elem = this.add_element(type,css_class,id,parent);
	elem.innerHTML = translation_thunk();
	this.meta_locale.add_display_message(elem,
			     translation_thunk,
			     true);
	
	return elem;
    }

    create_word_instruction_section(forwardp){
	
	var section=document.createElement("section");
	var h2 = this.add_element_and_translation("h2","","",section,
						  ()=>this.meta_locale.i18n.gettext("Word"));
	
	var ul=this.add_element("ul","","",section);
	var li1=this.add_element("li","","",ul);
	li1.innerHTML = this.word;	


	var li2=this.add_element("li","","",ul);
	var span21 = this.add_element_and_translation("span","bold","",li2,
						      () => this.meta_locale.i18n.gettext("Direction: "));
	
	if(forwardp){

	    var span22 = this.add_element_and_translation("span","","",li2,
							  () => this.meta_locale.i18n.gettext("Forward"));
	   
	}else{
	    
	    var span22 = this.add_element_and_translation("span","","",li2,
							  () => this.meta_locale.i18n.gettext("Backward"));
	    
	}
	
	return section;
    }

    create_numeric_instruction_section(forwardp,starting_number,delta){
	var section=document.createElement("section");
	var h2=this.add_element("h2","","",section);
	
	h2.innerHTML=this.meta_locale.i18n.gettext("Numerical sequence");
	this.meta_locale.add_display_message(h2,
						 () => this.meta_locale.i18n.gettext("Numerical sequence"),
						 true);
	
	var ul=this.add_element("ul","","",section);
	
	var li1=this.add_element("li","","",ul);

	var span11=this.add_element_and_translation("span","bold","",li1,
						    ()=>this.meta_locale.i18n.gettext("Direction: "));
	
	if(forwardp){
	    var span12=this.add_element_and_translation("span","","",li1,
							() => this.meta_locale.i18n.gettext("Forward"));
	}else{
	    var span12=this.add_element_and_translation("span","","",li1,
							() => this.meta_locale.i18n.gettext("Backward"));
	}
	
	var li2=this.add_element("li","","",ul);

	var span21 = this.add_element_and_translation("span","bold","",li2,
						      ()=> this.meta_locale.i18n.gettext("Starts: "));
	var span22 = this.add_element("span","","",li2);
	span22.innerHTML = starting_number;
	
	var li3=this.add_element("li","","",ul);

	var span31 = this.add_element_and_translation("span","bold","",li3,
						      ()=> this.meta_locale.i18n.gettext("Change: "));
	var span23 = this.add_element("span","","",li3);
	span23.innerHTML = delta;

	return section;
	
    }

    
    create_instructions_slide(starting_number,delta){
	var section1=this.create_numeric_instruction_section(this.increasing_sequence == "numeric",starting_number,delta);
	var section2=this.create_word_instruction_section(this.increasing_sequence == "word");
	var section1_html = section1.outerHTML;
	var section2_html = section2.outerHTML;
	var slide_html=`<div id="sections">
${section1_html}
${section2_html}
</div>`;
	return slide_html;
    }


    async set_up_cycle(){
	this.increasing_sequence=["numeric","word"][this.get_random_int(0,1)];
	await this.choose_word();
	
	this.sequences_length=this.word.length;
	this.numeric_sequence = this.generate_numeric_sequence(this.sequences_length);
	if(this.increasing_sequence == "numeric" ){

	    var word_elem = (i) => this.word.charAt(this.sequences_length -1 -i);
	    var num_elem = (i) => this.numeric_sequence[i] ;
	    var starting_number=this.numeric_sequence[0];
	    var delta=this.numeric_sequence[1] - this.numeric_sequence[0];
	} else{
	    var word_elem = (i) => this.word.charAt(i);
	    var num_elem = (i) => this.numeric_sequence[this.sequences_length -1 - i ] ;
	    var starting_number=this.numeric_sequence[this.sequences_length -1];
	    var delta=this.numeric_sequence[this.sequences_length -1] - this.numeric_sequence[this.sequences_length -2];	    
	}	

	var first_slide = () => {
	    var slide_html=this.create_instructions_slide(starting_number,delta);
	    this.message_peer(slide_html);
	    return slide_html;
	}
	
	this.enumerate_slides = Array.from({length : this.sequences_length},
					   (v,i)=> [word_elem(i),
						    num_elem(i),
						    i,
						    this.sequences_length]);

	
	this.enumerate_slides  = [first_slide].concat(this.enumerate_slides);

    }

    get_answer(index){
	var letter=this.enumerate_slides[index][0];
	var number=this.enumerate_slides[index][1];
	if(letter != undefined){
	    return `${number} ${letter}`;
	}else{
	    return "";
	}
    }

}

