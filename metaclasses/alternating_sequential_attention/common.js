"use strict";

class AlternatingSequentialAttention extends slidesExercise{
    constructor(){
	super();
	// The following variable corresponds to the slide div element
	// its contents are what is shown in the slide.
	// this.slide
    }

    init_vars(){
    
      this.meta_locale = new metaLocale();
    
      //** Define here attributes needed for patient and supervisor
      //** Also any attribute nedded for set_up_cycle() for the supervisor
    
      //** words or phrases that need to be translated must be
      //** used at least once in the following way
      // this.exercise_locale.i18n.gettext("LITERAL THING THAT NEEDS TO BE TRANSLATED");
    
    }


    //** Receives the slide parameters and sets the contents of this.slide
    show_slide(letter,number,i,n){
	var slide_content=`${i+1}/${n}`;
	if(i == 0){
	    slide_content = slide_content + `<br/><br/><br/><span class="first-message">${number}</span><span class="first-message">${letter}</span>`;
	}
	this.show_message(slide_content);
	//** Any element that needs to be translated whenever the
	//** language is changed
	//  this.exercise_locale.add_display_message(elem_var,
	//                                           () => {return var text=this.exercise_locale.i18n.gettext(thing_to_translate);},
	//                                           true)    
    }
}
