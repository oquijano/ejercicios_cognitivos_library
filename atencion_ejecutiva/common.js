"use strict";

function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1) + min); // The maximum is inclusive and the minimum is inclusive
}

class ExecutiveAttention extends slidesExercise{
    
    constructor(){	
	super();
	
	this.canvas = document.createElement("div");
	this.canvas.id = "canvas";

	this.slide.appendChild(this.canvas);

	this.create_circles();
	this.diagonal = this.add_element("div","diagonal","diagonal",this.slide);
	this.diagonal.display="none";	
    }

    init_vars(){
	this.exercise_locale = new exerciseLocale();
    }

    set_diameters(){
	this.circle_diameter = this.compute_circle_size(0.3);
	this.circle_letter.style.width = this.circle_diameter + "px";
	this.circle_letter.style.height = this.circle_diameter + "px";	
	this.circle_number.style.width = this.circle_diameter + "px";
	this.circle_number.style.height = this.circle_diameter + "px";
    }

    create_circles(){
	
	this.circle_letter = this.add_element("div","circle","circle_letter",this.canvas);
	this.circle_number = this.add_element("div","circle","circle_number",this.canvas);


	this.letter = this.add_element("div","circle_text","letter",this.circle_letter);
	this.number = this.add_element("div","circle_text","number",this.circle_number);
	this.disappear_circles();
    }

    disappear_circles(){
	this.circle_letter.style.display="none";
	this.circle_number.style.display="none";
    }

    appear_circles(){
	this.circle_letter.style.display="";
	this.circle_number.style.display="";
    }

    put_diagonal(){

	// let center1 = this.get_circle_center(this.circle_letter);
	// let center2 = this.get_circle_center(this.circle_number);

	// let line_length = this.compute_circle_size(0.8);

	// this.diagonal.style.width=`${line_length}px`;

	// let offset_info=this.canvas.getBoundingClientRect();
	// let x_offset=offset_info.x;
	// let y_offset=offset_info.y;

	// let middle_point = this.middle_point(center1,center2);	
	//	let x = Math.floor(middle_point[0] - line_length/2 );
	let brect=this.canvas.parentElement.getBoundingClientRect();
	let line_length=Math.min(brect.width,brect.height);
	this.diagonal.style.width=`${line_length}px`;

	let x = Math.floor(brect.width / 2 - line_length/2);
	let y = Math.floor(brect.height / 2);

	let angle = getRandomIntInclusive(0,180);	
	// let angle = Math.floor(this.get_angle(center1,center2));

//	this.diagonal.style.top = `${y}px`;
//	this.diagonal.style.left = `${x}px`;


	this.diagonal.style.transform = `translate(${x}px,${y}px) rotate(${angle}deg)`;
    }

    compute_circle_size(reference_proportion){
	let radius_reference = Math.min(this.canvas.clientWidth,this.canvas.clientHeight);
	let circle_size = Math.floor( radius_reference * reference_proportion);
	return circle_size;
    }

    get_circle_center(a_circle){
	let rext_info=a_circle.getBoundingClientRect();
	let x = (rext_info.x + rext_info.right)/2;
	let y = (rext_info.y + rext_info.bottom)/2;
	return [x,y];
    }

    middle_point(center1,center2){
	let x = (center1[0] + center2[0])/2;
	let y = (center1[1] + center2[1])/2;
	return [x,y];
    }

    get_angle(center1,center2){
	let x_diff = center2[0] - center1[0];
	let y_diff = center2[1] - center1[1];
	let slope=0;
	
	if (x_diff){
	    slope = y_diff/x_diff;
	}


	let angle = Math.atan(-slope) * 180 / Math.PI;

	if(! y_diff){
	    angle=90;
	}
	
	return  angle;
    }

    index_to_coordinates(an_index){
	// grid values go from 1 to 16 countring from left to right
	// it converts a number from 1 to 16 to the row and column in the grid
	let row = Math.ceil(an_index / 4);
	let column = an_index - 4*(row-1);
	return [row,column];
    }

    show_slide(index_letter,letter,index_number,number,diagonal){
	this.set_diameters();
	this.appear_circles();
	this.diagonal.style.display="none";

	let letter_coordinates = this.index_to_coordinates(index_letter);
	this.circle_letter.style.gridRow = letter_coordinates[0];
	this.circle_letter.style.gridColumn = letter_coordinates[1];
	this.letter.innerText = letter;
	let letter_height = this.letter.getBoundingClientRect().height;
	this.letter.style.top = Math.floor((this.circle_diameter -letter_height)/2) + "px";

	let number_coordinates = this.index_to_coordinates(index_number);
	this.circle_number.style.gridRow = number_coordinates[0];
	this.circle_number.style.gridColumn = number_coordinates[1];
	this.number.innerText= number;
	this.number.style.top = Math.floor((this.circle_diameter -letter_height)/2) + "px";
	

	if(diagonal){
	    this.put_diagonal();
	    this.diagonal.style.display="";
	}

	
	
    }

    
}
