"use strict";

class Supervisor extends ExecutiveAttention{
    constructor(){
	super();
	this.add_controls();
	this.Ncycles = 3;
	this.min_slide=1;
	
    }


    random_different_index(index){
	// Returns  random element from [0,1,2,3,4] different than index
	let remaining = [...Array(16).keys()];
	remaining.splice(index,1);
	return remaining[this.get_random_int(0,14)]+1;
    }

    random_letter(){
	return this.letters_array[this.get_random_int(0,25)];	
    }

    random_number(){
	return this.get_random_int(1,99);
    }

    generate_slide(index){
	let letter_index = this.get_random_int(1,16);
	let number_index = this.random_different_index(letter_index-1);
	let letter = this.random_letter();
	let number = this.random_number();
	if(this.diagonal_slides.includes(index)){
	    return [letter_index,letter,number_index,number,true];
	}else{
	    return [letter_index,letter,number_index,number,false];
	}	
    }


    set_up_cycle(){
	this.Nslides = 45;
	this.diagonal_slides=[8,20,27,35,43];
	this.letters_array='ABCDEFGHIJKLMNOPQRSTUVWXYZ';

	this.enumerate_slides=[];
	this.answers=[];
	let random_choice=this.get_random_int(0,1);
	if(random_choice==0){
	    this.current_answer="number";
	    this.enumerate_slides[0] = () => {
		var text=this.exercise_locale.i18n.gettext("Number starts");
		this.message_peer(text);
		return text;
	    };
	    this.answers[0]="";
	}else{
	    this.current_answer="letter";
	    this.enumerate_slides[0] = () => {
		var text=this.exercise_locale.i18n.gettext("Letter starts");
		this.message_peer(text);
		return text;
	    };
	    this.answers[0]="";
	}
	
	for(let slide=1; slide <= this.Nslides; slide++){
	    this.enumerate_slides[slide] = this.generate_slide(slide);
	    
	    let letter = this.enumerate_slides[slide][1];
	    let number = this.enumerate_slides[slide][3];
	    if(this.current_answer == "number"){
		this.answers[slide] = number + " " + letter;
	    }else{
		this.answers[slide] = letter + " " + number;
	    }
	    if(this.diagonal_slides.includes(slide)){
		if(this.current_answer == "number"){
		    this.current_answer="letter";
		}else{
		    this.current_answer="number";
		}
	    }
	    
	}	
    }

    get_answer(index){
	return this.answers[index];
    }
}

var lala = new Supervisor();
