"use strict";

class Supervisor extends numericPattern{
    constructor(){
	super();
	
	this.add_controls(true,true,true);
	
	this.Ncycles = 1;

	//** At which slide should the clock start running
	//** it corresponds to an index of enumerate_slides
	//** default value: 0
	this.min_slide=1;
	this.exercise_locale.add_display_message(this.answer , () => this.get_answer(this.slide_number),true);
    }

    inverse_operation(numbers_array){
	return [numbers_array[0]-16,numbers_array[1]+12,numbers_array[2]/2]
    }

    operation(numbers_array){
	return [numbers_array[0]+16,numbers_array[1]-12,numbers_array[2]*2]
    }

    get_min_index(array){
	var elems_and_index=Array.from(array,(e,i)=>[e,i]);

	return elems_and_index.reduce(
	    (min,curr)=>{
		if(curr[0]<min[0])
		    return curr;
		else
		    return min;
	    },
	    [Infinity,-1])[1];
	
    }

    get_max_index(array){
	var elems_and_index=Array.from(array,(e,i)=>[e,i]);

	return elems_and_index.reduce(
	    (max,curr)=>{
		if(curr[0]>max[0])
		    return curr;
		else
		    return max;
	    },
	    [-Infinity,-1])[1];
	
    }



    generate_slide_and_answer(){

	var lowestp=this.get_random_int(0,1);

	var range_options=[0,1,2,3,4];
		
	var starting_number=2*this.get_random_int(8,100);
	
	var third_number=2*this.get_random_int(0,2);
	range_options.splice(third_number,1);
	var index=this.get_random_int(0,3);
	var second_number=range_options[index];
	range_options.splice(index,1);
	index=this.get_random_int(0,2);
	var first_number=range_options[index];
	
	var inverse_params=[starting_number+first_number,
			    starting_number+second_number,
			    starting_number+third_number];

	if(! lowestp){
	    var answer=this.get_min_index(inverse_params);
	}else{
	    var answer=this.get_max_index(inverse_params);
	}

	var slide_params=this.inverse_operation(inverse_params);

	return {slide: slide_params.concat(lowestp),
		answer: answer}	       	
    }

    set_up_cycle(){
	
	var params_and_answers=Array.from({length: 24},(e,i)=>this.generate_slide_and_answer());
	this.enumerate_slides=[()=>{
	    var text=`${this.exercise_locale.i18n.gettext("Operations")}<br/>(+16,-12,x2)`;
	    if(this.slide_number == 1){
		this.message_peer(text);
	    }
	    return text;
	}];
	this.answers=[""];
	
	this.enumerate_slides=this.enumerate_slides.concat(params_and_answers.map(x=>x.slide));
	this.answers=this.answers.concat(params_and_answers.map(x=>x.answer));
	
    }

    get_answer(index){
	if(index<this.min_slide )
	    return "";
	else
	    return this.exercise_locale.i18n.gettext(this.answer_text[this.answers[index]]);
    }

}
var lala=new Supervisor();
