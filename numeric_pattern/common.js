"use strict";

class numericPattern extends slidesExercise{
    constructor(){
	super();
    }

    init_vars(){
    
	this.exercise_locale = new exerciseLocale();
	
	this.which_text=[this.exercise_locale.i18n.gettext("Lowest"),
			 this.exercise_locale.i18n.gettext("Highest")];
	
	this.answer_text=[this.exercise_locale.i18n.gettext("Left"),
			  this.exercise_locale.i18n.gettext("Center"),
			  this.exercise_locale.i18n.gettext("Right")];
	
    }


    // which ==0 means lowest one
    // which ==1 means largest one
    show_slide(n1,n2,n3,which){

	this.slide.innerHTML="";
	var line1=this.add_element("div","line1","line1",this.slide);	
	var line2 =this.add_element("div","line2","line2",this.slide);
	line1.innerHTML=`(${n1},${n2},${n3})`;

	
	this.exercise_locale.add_display_message(line2,
						 () => this.exercise_locale.i18n.gettext(this.which_text[which]),
											 true);					
    }
}
