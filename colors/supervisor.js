"use strict";


class Supervisor extends Colors{

    constructor(){
	super();	
	// selected_box: value 1 means the selected color is in box1
	//               value 2 means the selected color is in box2
	this.selected_box=null;
	// Index of the color used for the operation
	this.selected_color = null;
	
	this.add_controls();
	
	this.colors_operations = [(x) => {return x +2 }, (x) => {return x*2}, (x) => {return x-2}, (x) => {return x/2} ];

	this.Ncycles = 3;
	
    }


    distribute(Nslides,Numbers){
	// Numbers is an array of integers
	// Nslides is the number of slides
	let Nelems = Numbers.length;
	let base = Math.floor(Nslides / Nelems);
	let counts = Numbers.map(x => base);
	while( counts.reduce( (x,y) => x+y) < Nslides  ){
	let index = this.get_random_int(0,Nelems-1);
	counts[index]++;
	}
	return counts;
    }

    random_different_index(index){
	// Returns  random element from [0,1,2,3,4] different than index
	let remaining = [0,1,2,3];
	remaining.splice(index,1);
	return remaining[this.get_random_int(0,2)];
    }


    next_state(){
	
	// this.disappear_()

	if(this.selected_color == null) {
	    this.selected_color = this.get_random_int(0,3);
	}else{
	    this.selected_color = this.random_different_index(this.selected_color);
	}

	let other_color = this.random_different_index(this.selected_color);

	let selected_color_position = this.get_random_int(0,1);

	if (selected_color_position == 0){
	    return [this.selected_color, other_color, selected_color_position];
	}else{
	    return [other_color, this.selected_color, selected_color_position];
	}
		
    }
    

    generate_cycle(Numbers){
	    
	let slide = 0;
	let total_slides = 60;
	this.enumerate_slides =[];
	this.changing_number = [];
	let counts = this.distribute(total_slides,Numbers);
	
	for(let i=0; i<Numbers.length;i++){
	    this.enumerate_slides[slide] = Numbers[i].toString();
	    this.changing_number[slide] = Numbers[i];
	    slide++;
	    for(let j=0; j<counts[i];j++){
		this.enumerate_slides[slide] = this.next_state();
		this.changing_number[slide] = Numbers[i];
		slide++;
	    }}
    }

    set_up_cycle(){
	if(this.cycle == 1){
	    this.generate_cycle([20,30,40,50,60,70,80,90]);
	}else if(this.cycle == 2){
	    this.generate_cycle([20,30,40,50,60,70,80,90,100]);
	}else{
	    this.generate_cycle([20,30,40,50,60,70,80,90,100,110,120]);
	}
    }

    get_answer(index){	
	let current_state = this.enumerate_slides[index];
	if( typeof(current_state) == "string" && this.answer != undefined ){
	    return "";
	}else{
	    let color_number = current_state[current_state[2]];
	    return this.colors_operations[color_number](this.changing_number[index]);
	}
    }
    

}


var lala=new Supervisor();


