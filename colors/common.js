"use strict";

class Colors extends slidesExercise{
    constructor(){
	super();
	this.slide.innerHTML=`
       <div class="color-boxes" id="colors">       
       <div class="color-box" id="box1"></div>
       <div class="color-box" id="box2"></div>      
       </div>`;

	this.colors = ["#0000ee","#eeee00","#ee0000","#00aa00"];
	// id1 is the id of the left block
	// id2 is the id of the right block  
	this.box1 = document.getElementById("box1");
	this.box2 = document.getElementById("box2");	

	this.circle = document.getElementById("circle");
	this.message = document.getElementById("message");

	this.disappear_slide();	
    }

    
    set_circle(index){	
	if( this.circle != null ){
	    this.circle.remove();
	}
	
	this.circle = document.createElement("div");
	this.circle.setAttribute("id","circle");

	if(index == 0){
	    this.box1.appendChild(this.circle);
	}else{
	    this.box2.appendChild(this.circle);
	}
    }


    show_slide(color_index1,color_index2,selection){
	// color_index1, color_index2 numbers between 0 and 3
	// selection : 0 , 1
	this.box1.style.display="";
	this.box2.style.display="";
	this.box1.style.backgroundColor = this.colors[color_index1];
	this.box2.style.backgroundColor = this.colors[color_index2];
	this.set_circle(selection);
    }
    
}

