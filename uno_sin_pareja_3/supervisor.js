"use strict";

var lala;

function start_supervisor(){
    class Supervisor extends Supervisor_Meta{

	constructor(){
	    super(...class_params);
	    this.Ncycles=3;
	}

	init_vars(){
	    this.border_size=5;
	    this.grid_size= class_params[0] * class_params[1];
	    this.cycles_info=[{ number_of_images : 15, Nimages : [10,10,9,9,10,9,9,9,9,11,10,10,10,11,9,11,11,11] },
			      { number_of_images : 11, Nimages : [9,9,9,9,9,9,9,9,8,9,9,9,9,9,11,11,11,11]},
			      { number_of_images : 8, Nimages : [7,7,7,7,7,8,7,8,7,8,8,8,8,7,8,8,7,8] }]
	    // in the third cycle, rotation start in slide 9 (position 8)
	    this.exercise_locale = new exerciseLocale();
	    this.credits=this.exercise_locale.i18n.gettext("Pictograms from <a href=\"https://mulberrysymbols.org/\">Mulberry Symbols</a>. <br/>Copyright 2018/19 Steve Lee <br/> License: <a href=\"https://creativecommons.org/licenses/by-sa/4.0/\"> CC BY-SA</a>");
	}		
    }

    lala = new Supervisor();
}


loadMetaSupervisor("uno_sin_pareja",
		   true,
		   true,
		   start_supervisor)
