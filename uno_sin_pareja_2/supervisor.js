"use strict";

var lala;

function start_supervisor(){

    class Supervisor extends Supervisor_Meta{

	constructor(){
	    super(...class_params);
	    this.Ncycles=3;
	}

	init_vars(){
	    this.border_size=5;
	    this.grid_size=class_params[0]*class_params[1];
	    this.cycles_info=[{ number_of_images : 15 , Nimages : [6,8,8,8,6,8,9,6,8,7,8,8,8,10,10,8,9,10] },
			      { number_of_images : 16 , Nimages : [9,10,9,9,9,9,8,8,8,7,9,10,8,11,11,13,14,16]},
			      { number_of_images : 16  , Nimages : [8,11,8,8,11,10,11,9,10,11,10,16,16,10,12,8,8,10] }]
	    // in the third cycle, rotation start in slide 9 (position 8)
	    this.exercise_locale = new exerciseLocale();
	    this.credits=this.exercise_locale.i18n.gettext("Pictograms from <a href=\"https://mulberrysymbols.org/\">Mulberry Symbols</a>. <br/>Copyright 2018/19 Steve Lee <br/> License: <a href=\"https://creativecommons.org/licenses/by-sa/4.0/\"> CC BY-SA</a>");
	}
	
    }

    lala = new Supervisor();
    
}

loadMetaSupervisor("uno_sin_pareja",
		   true,
		   true,
		   start_supervisor);
