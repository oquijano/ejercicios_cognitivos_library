"use strict";
var lala;

function start_supervisor(){
    class Supervisor extends Supervisor_Meta{
	constructor(){
	    super(...class_params);
	    this.Ncycles=1;
	}

	init_vars(){
	    this.border_size=5;
	    this.grid_size=25;
	    this.cycles_info=[{ number_of_images : 41 , Nimages : [5,6,6,6,6,6,6,5,6,7,6,11,10,9,9,8,9,10,10,7,7,6,6,6,5 ] }]
	    this.exercise_locale = new exerciseLocale();
	    this.credits=this.exercise_locale.i18n.gettext("Pictograms from <a href=\"https://mulberrysymbols.org/\">Mulberry Symbols</a>. <br/>Copyright 2018/19 Steve Lee <br/> License: <a href=\"https://creativecommons.org/licenses/by-sa/4.0/\"> CC BY-SA</a>");
	}
    }
    
    lala=new Supervisor();
}

		     
loadMetaSupervisor("uno_sin_pareja",
		   true,
		   true,
		   start_supervisor);
