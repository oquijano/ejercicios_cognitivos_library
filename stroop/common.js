"use strict";

class Stroop extends slidesExercise{
    constructor(){
	super();	
	this.color_text=this.add_element("div","color","color",this.slide);
	this.color_text.classList.add("text");
    }

    init_vars(){
	this.exercise_locale = new exerciseLocale();	
	this.start_message=[() => {
	    var text=this.exercise_locale.i18n.gettext("Instructions:<br/>Underline: Say color of the letters.<br/>Not underlined: Say written name." );
	    return text;
	}];

	
	this.color_names=[
	    this.exercise_locale.i18n.gettext("GREEN"),
	    this.exercise_locale.i18n.gettext("BLACK"),
	    this.exercise_locale.i18n.gettext("YELLOW"),
	    this.exercise_locale.i18n.gettext("PINK"),
	    this.exercise_locale.i18n.gettext("BLUE"),
	    this.exercise_locale.i18n.gettext("PURPLE"),	    	    	    
	];

	this.color_hexcodes={"GREEN" : "#00BB00",
			     "BLACK" : "#000000",
			     "YELLOW" : "#EEEE00",
			     "PINK" : "#FFC0CB",
			     "BLUE" : "#0000FF",
			     "PURPLE" : "#8800C7"};
    }

    show_slide(color_name_index,font_color_index,stroop){
	// Both color_name and font_color have to be integers that
	// correspond to indexes of this.color_names .
	let color_name=this.color_names[color_name_index];
	let color_hex=this.color_hexcodes[this.color_names[font_color_index]];	
	this.exercise_locale.add_display_message(this.color_text,() => {
	    var text=this.exercise_locale.i18n.gettext(color_name);
	    return text;
	},true);
	this.color_text.style=`color: ${color_hex};`;
	if(stroop){
	    this.color_text.classList.add("underline");
	}else{
	    this.color_text.classList.remove("underline");
	}
    }
}
