"use strict";

function arr_equal(a,b){

    if( a.length == b.length ){
	for(let i=0; i<a.length ; i++){
	    if( a[i] != b[i] ){
		return false;
	    }}
	return true;
    }else{
	return false;
    }
    
}

class Supervisor extends Stroop{
    constructor(){
	super();
	this.add_controls();
	this.Ncycles = 1;
	this.min_slide=1;
    }

    get_random_color_index(){
	return this.get_random_int( 0 , this.color_names.length - 1);
    }


    colors_list_html(){
	var color_list_html=`<div id="title">${this.exercise_locale.i18n.gettext("Colors")}:</div><ul style="list-style-type: none;">\n`;
	for(let i=0; i<this.color_names.length;i++){
	    let color_name = this.color_names[i];
	    let color_code = this.color_hexcodes[color_name];
	    color_list_html += `<li style="color: ${color_code}; font-size: 4vh;font-weight: bold;">${this.exercise_locale.i18n.gettext(color_name)}</li>`;
	}	
	color_list_html+="<ul>\n";
	return color_list_html;
	
    }

    generate_slide(previous_slide=null){
	if(previous_slide === null){
	    return [this.get_random_color_index(),
		    this.get_random_color_index(),
		    [true,false][this.get_random_int( 0 , 1)]
		   ];
	}else{
	    var new_slide = this.generate_slide();
	    while( arr_equal(new_slide,previous_slide) ){
		new_slide = this.generate_slide();
	    }
	    return new_slide;
	}
    }

    
    
    set_up_cycle(){
	let Nslides=125;
	this.enumerate_slides=[() => {
	    var text=this.colors_list_html();
	    if(this.slide_number ==1){
		this.message_peer(text);
	    }
	    return text;
	}];
	for(let i=1; i<=Nslides; i++){
	    if(i > 1){
		this.enumerate_slides[i] = this.generate_slide(this.enumerate_slides[i-1]);
	    }else{
		this.enumerate_slides[i] = this.generate_slide();
	    }
	}


    }

    get_answer(index){
	if(index > 0){
	    let params=this.enumerate_slides[index];
	    if(params[2]){
		return this.exercise_locale.i18n.gettext(this.color_names[params[1]]);
	    }else{
		return this.exercise_locale.i18n.gettext(this.color_names[params[0]]);
	    }
	}else{
	    return "";
	}
    }
}

var lala = new Supervisor() ;
