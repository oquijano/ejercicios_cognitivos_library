"use strict";


function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1) + min); // The maximum is inclusive and the minimum is inclusive
}

function random_element (arr) {
    return arr[getRandomIntInclusive(0,arr.length-1)];
}



// color_value is one of: "red","green", "yellow", "blue"

// Elements to be displayed in the grid should be subclasses of
// each element and they should define the following methods:
// display(value)
// generate_random_value()
class gridElement{
    
    constructor(type,css_class,id,parent){	
	this.element = this.add_element(type,css_class,id,parent);
	this.hide();
    }

    add_element(type,css_class,id,parent){
	let elem = document.createElement(type);
	elem.className = css_class;
	if(id!=null)
	    elem.id = id;
	parent.appendChild(elem);
	return elem;
    }

    index_to_coordinates(an_index){
	// grid values go from 1 to 16 countring from left to right
	// it converts a number from 1 to 16 to the row and column in the grid
	let row = Math.ceil(an_index / 4);
	let column = an_index - 4*(row-1);
	return [row,column];
    }

    place(grid_index){
	let coordinates = this.index_to_coordinates(grid_index);
	this.element.style.gridRow = coordinates[0];
	this.element.style.gridColumn = coordinates[1];	
    }

    hide(){
	this.element.style.display="none";
    }

    show(value=undefined,index=undefined){
	if(index != undefined){
	    this.place(index);
	}
	if(value != undefined){
	    this.display(value);
	}else{
	    this.display();
	}
    }
    
}

class Circle extends gridElement{
    constructor(parent,exercise_locale){
	super("div","circle","color",parent);
	// this.color_codes={"Rojo" : "#FF0000",
	// 		  "Verde" : "#00FF00",
	// 		  "Amarillo" : "#FFFF00",
	// 		  "Azul" : "#0000FF"}
	this.exercise_locale = exercise_locale;
	this.color_codes = {}
	this.color_codes[this.exercise_locale.i18n.gettext("Red")]="#FF0000";
	this.color_codes[this.exercise_locale.i18n.gettext("Green")]="#00FF00";
	this.color_codes[this.exercise_locale.i18n.gettext("Yellow")]="#FFFF00";
	this.color_codes[this.exercise_locale.i18n.gettext("Blue")]="#0000FF";

    }

    generate_random_value(){
	return random_element(Object.keys(this.color_codes));
    }

    set_diameter(){
	this.circle_diameter = this.compute_circle_size(0.4);
	this.element.style.width = this.circle_diameter + "px";
	this.element.style.height = this.circle_diameter + "px";	
    }

    display(value){
	this.element.style.background=this.color_codes[value];
	this.element.style.display="";
    }

    compute_circle_size(reference_proportion){
	let radius_reference = Math.min(this.element.parentElement.clientWidth,this.element.parentElement.clientHeight);
	let circle_size = Math.floor( radius_reference * reference_proportion);
	return circle_size;
    }
    
}


class Letter extends gridElement{
    
    constructor(parent){
	super("div","text","letter",parent);
	this.letters_array='ABCDEFGHIJKLMNÑOPQRSTUVWXYZ';
    }

    generate_random_value(){
	return random_element(this.letters_array);
    }

    display(value){
	this.element.innerHTML=value;
	this.element.style.display="";
    }
    
}

class Numero extends gridElement{
    
    constructor(parent){
	super("div","text","number",parent);
    }

    generate_random_value(){
	return getRandomIntInclusive(1,99);
    }

    display(value){
	this.element.innerHTML=value;
	this.element.style.display="";
    }
    
}

class Direction extends gridElement{
    constructor(parent,exercise_locale){
	super("div","text","direction",parent);
	// this.arrow_codes={"Arriba" : "&#x2191;",
	// 		  "Abajo" : "&#x2193;",
	// 		  "Izquierda" : "&#x2190;",
	// 		  "Derecha" : "&#x2192;"};
	this.exercise_locale = exercise_locale;
	this.arrow_codes={};
	this.arrow_codes[this.exercise_locale.i18n.gettext("Up")]= "&#x2191;" ;
	this.arrow_codes[this.exercise_locale.i18n.gettext("Down")]= "&#x2193;" ;
	this.arrow_codes[this.exercise_locale.i18n.gettext("Left")]= "&#x2190;" ;
	this.arrow_codes[this.exercise_locale.i18n.gettext("Right")]= "&#x2192;" ;
	
	
    }

    generate_random_value(){
	return random_element(Object.keys(this.arrow_codes));
    }

    display(value){
	this.element.innerHTML = this.arrow_codes[value];
	this.element.style.display="";
    }
}

class Diagonal extends gridElement{

    constructor(parent){
	super("div","diagonal","diagonal",parent);
    }
    
    display(value){
	if(value){
	    let brect=this.element.parentElement.getBoundingClientRect();	
	    let line_length=Math.min(brect.width,brect.height);
	    this.element.style.width=`${line_length}px`;	
	    let x = Math.floor(brect.width / 2 - line_length/2);
	    let y = Math.floor(brect.height / 2);
	    let angle = getRandomIntInclusive(0,180);	
	    this.element.style.transform = `translate(${x}px,${y}px) rotate(${angle}deg)`;
	    this.element.style.display="";
	}else{
	    this.hide();
	}
    }
    
}


class AtencionEjecutivaTrailCuatro extends slidesExercise{
    
    constructor(){
	super();
	this.define_grid_elements();
	
    }

    init_vars(){
	this.exercise_locale = new exerciseLocale();
    }


    define_element(elem_class,locale){
	if (locale === undefined){
	    return new elem_class(this.canvas);
	}else{
	    return new elem_class(this.canvas,locale);
	}
    }    

    define_grid_elements(){
	
	if(this.grid_elements == undefined){
	    this.canvas = document.createElement("div");
	    this.canvas.id = "canvas";

	    this.slide.appendChild(this.canvas);
	    
	    // this.grid_elements = {"Color" : this.define_element(Circle),
	    // 			  "Dirección" : this.define_element(Direction),
	    // 			  "Número" : this.define_element(Numero),
	    // 			  "Letra" : this.define_element(Letter),
	    // 			  "diagonal" : this.define_element(Diagonal)};
	    	    

	    this.grid_elements = {};
	    this.grid_elements[this.exercise_locale.i18n.gettext("Color")] = this.define_element(Circle,this.exercise_locale);
	    this.grid_elements[this.exercise_locale.i18n.gettext("Direction")] = this.define_element(Direction,this.exercise_locale);
	    this.grid_elements[this.exercise_locale.i18n.gettext("Number")] = this.define_element(Numero);
	    this.grid_elements[this.exercise_locale.i18n.gettext("Letter")] = this.define_element(Letter);
	    this.grid_elements["diagonal"] = this.define_element(Diagonal);

	    this.elements_names = Object.keys(this.grid_elements).filter((x) => x!== "diagonal");
	}
	
    }        

            

    // The slide should be an object with the following entries
    // {"color" : [color_value,index],
    //  "direction" : [direction_value,index],
    //  "number" : [number_value,index],
    //  "letter" : [letter_value,index]}
    show_slide(slide_info){
	
	for(let elem in this.grid_elements){
	    this.grid_elements[elem].show( ...slide_info[elem] )
	}	
    }
}
