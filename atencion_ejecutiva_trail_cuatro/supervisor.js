"use strict";

function random_sample(arr,Nelems){
    let arr_copy=[...arr];
    let Narr=arr_copy.length;
    let ret=[];
    if(Nelems > Narr){
	throw new Error("Sample size is greater than the number of elements in the array");
    }
    for(let i=0;i<Nelems;i++){
	ret=ret.concat(arr_copy.splice( getRandomIntInclusive(0,Narr - 1) , 1));
	Narr=arr_copy.length;
    }
    return ret;
}

function shuffle(arr){
    let arr_copy=[...arr];
    return random_sample(arr_copy,arr_copy.length);
}

class Supervisor extends AtencionEjecutivaTrailCuatro{
    constructor(){
	super();
	this.add_controls();
	this.answer_label.innerText="";
	this.Ncycles = 3;
//	this.add_start_choice();
	this.min_slide=1;
	this.exercise_locale.add_display_message(this.answer , () => this.get_answer(this.slide_number),true);
    }

    init_vars(){
	super.init_vars();
	this.diagonal_slides=[4,8,11,15,19,23,27,31,35,39,43];
	this.define_grid_elements();
    }
	

    
    generate_slide(index){
	let ret_slide={};
	let indices=random_sample( new Array(16).fill(1).map( (_, i) => i+1 ) , 4 );
	for (let elem of Object.keys(this.grid_elements) ){
	    if(elem !== "diagonal"){
		ret_slide[elem] = [this.grid_elements[elem].generate_random_value(),
				   indices.pop()];
		    
	    }else{
		ret_slide[elem] = [this.diagonal_slides.includes(index)];
	    }
	}
	return ret_slide;
    }


    set_up_cycle(){

	this.Nslides = 45;

	this.enumerate_slides=[];
	this.answers=[];

	this.answers[0]=() =>"";

	this.order=shuffle(this.elements_names);
	
	this.enumerate_slides[0] = () => {	    
	    var text = this.order.map(x => this.exercise_locale.i18n.gettext(x)).join(" ");
	    if(this.slide_number == 1){
		this.message_peer(text);
	    }
	    return text;
	}
	
	
	
	for(let slide=1; slide <= this.Nslides; slide++){
	    let new_slide = this.generate_slide(slide);
	    this.enumerate_slides[slide] = [new_slide];
	    
	    if( this.diagonal_slides.includes(slide-1) ){
		// this.order.reverse();
		this.answers[slide] = () => {		
		    return this.order.reverse().map(
			(elem) =>  {			
			    try{
				return this.exercise_locale.i18n.gettext(new_slide[elem][0]);
			    }catch(error){
				return new_slide[elem][0];
			    }
			}).join(" ");
		}
		
	    }else{
		this.answers[slide] = () => {		
		    return this.order.map(
			(elem) =>  {			
			    try{
				return this.exercise_locale.i18n.gettext(new_slide[elem][0]);
			    }catch(error){
				return new_slide[elem][0];
			    }
			}).join(" ");
		}
		
	    }
	    
	    
	    
	}
    }

    get_answer(index){
	if(this.answers!=undefined)
	    return this.answers[index]();
    }
}

var lala = new Supervisor();
