"use strict";

class Supervisor extends Symbols{
    constructor(){
	super();
	this.add_controls();
	this.Ncycles = 3;
	this.min_slide=1;

    }

    random_index_except(an_index){
	let possible_values = Array.from({length: 10}, (_,i) => i);
	possible_values.splice(an_index,1);
	return possible_values[this.get_random_int(0,8)];
    }

    set_up_cycle(){
	// -1 shows the table of symbols with the number that
	// corresponds to each.
	let slides_per_cycle = 100;
	let table_slide=-1;
	
	this.enumerate_slides =[];
	this.enumerate_slides[0]=[table_slide];

	let all_indices = [0,1,2]
	for(let i=1; i<=slides_per_cycle; i++){
	    if(i==1){
		this.enumerate_slides[i] = [this.get_random_int(0,9)];
	    }else{
		this.enumerate_slides[i] = [this.random_index_except(this.enumerate_slides[i-1][0])];
	    }
	}
    }

    get_answer(index){
	if(index==0)
	    return "";
	else{
	    return this.enumerate_slides[index];
	}
	// let current_state = this.enumerate_slides[index];
	// if( typeof(current_state) == "string" && this.answer != undefined ){
	//     return "";
	// }else{	    
	//     return  current_state;
	// }
    }
}

var lala = new Supervisor() ;
