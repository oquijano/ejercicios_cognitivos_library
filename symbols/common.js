"use strict";

class Symbols extends slidesExercise{

    constructor(){
	super();
	this.displaying_table=false;
	this.slide.innerHTML=`
        <div class="symbol" id="symbol">
        </div>`;

	this.current = document.getElementById("symbol");

	this.symbols = ["&#8811;","%","&#916;","&#8593;","&#8721;","&#8800;","&#8734;","&#8962;","&#8873;","&#9788;"];

	this.symbols_table=document.createElement("table");

	this.symbols_table.innerHTML = `<tbody> <tr>
    <td >%</td>
    <td >&#916;</td>
    <td >&#8593;</td>
    <td >&#8721;</td>
    <td >&#8800;</td>
    <td >&#8734;</td>
    <td >&#8962;</td>
    <td >&#8873;</td>
    <td >&#9788;</td>
    <td >&#8811;</td>
  </tr>
  <tr>
    <td >1</td>
    <td >2</td>
    <td >3</td>    
    <td >4</td>
    <td >5</td>
    <td >6</td>
    <td >7</td>
    <td >8</td>
    <td >9</td>
    <td >0</td>
  </tr></tbody>`;	
    }

    show_slide(number){	
	if(number == -1){
	    this.slide.appendChild(this.symbols_table);
	    this.displaying_table=true;
	    this.current.innerHTML="";
	}
	else{
	    if(this.displaying_table){
		this.slide.removeChild(this.symbols_table);
		this.displaying_table=false;
	    }
	    this.current.innerHTML = this.symbols[number];
	}
    }
    
}
